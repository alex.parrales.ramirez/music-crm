<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('crm.dashboard');
    }
    public function clientes()
    {
        $clientes = User::get();
        return view('crm.clientes', compact('clientes'));
    }
    public function ventas()
    {
        return view('crm.ventas');
    }
    public function tareas()
    {
        return view('crm.tareas');
    }
    public function agenda()
    {
        return view('crm.agenda');
    }
}
