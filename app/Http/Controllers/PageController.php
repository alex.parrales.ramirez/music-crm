<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Album;
use App\Artista;
use App\Cancion;
use App\Venta;
class PageController extends Controller
{
    public function index()
    {
        $album_slide = Album::take(3)->get();
        $album_new = Album::where('id', '=', '5')
                            ->orwhere('id', '=', '6')
                            ->orwhere('id', '=', '7')
                            ->orwhere('id', '=', '8')
                            ->get();

        $album_tendencia = Album::take(9)->get();
        $album_top = Album::take(5)->get();
        $album_recommended = Album::take(8)->get();
        return view('index', compact('album_slide', 'album_new', 'album_tendencia', 'album_top', 'album_recommended'));
    }
    public function album($id)
    {
        $album = Album::findOrFail($id);
        $album_top = Album::take(5)->get();
        $album_related = Album::take(4)->get();
        $canciones = Cancion::where('album_id','=', $id)->get();
        return view('album' ,compact('album','canciones', 'album_top'));
    }
    public function albums()
    {
        $albums = Album::get();
        $album_top = Album::take(5)->get();
        return view('albums' ,compact('albums','album_top'));
    }
    public function artista($id){

        $albums = Album::where('artista_id','=',$id)->get();
        $artista = Artista::findOrFail($id);
        $album_top = Album::take(5)->get();
        return view('artista', compact('albums','artista','album_top'));
    }
    public function artistas()
    {
        $artistas = Artista::get();
        $album_top = Album::take(5)->get();
        return view('artistas', compact('artistas','album_top'));
    }


   public function venta($album_id)
   {
    $album = Album::findOrFail($album_id);
      return view('venta', compact('album'));  
   }
//    $user_id = Auth::id();
//         $album = Album::findOrFail($album_id);
//         $monto = $album->precio;
//         DB::table('ventas')->insert([
//             'user_id' => $user_id,
//             'album_id' => $album_id,
//             'monto' => $monto,
//         ]);
//         return back();
}
