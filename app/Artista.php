<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    protected $fillable= [
        'nombre',
    ];

    public function albums(){
		return $this->hasMany(Album::class);
	}
}
