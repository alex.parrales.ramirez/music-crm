<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'artista_id',
        'nombre',
        'anio',
        'imagen',
        'precio',
    ];
    public function artista(){
    	return $this->belongsTo(Artista::class);
    }
}
