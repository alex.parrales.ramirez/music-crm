@extends('layouts.app')
@section('content')

<div id="content" class="app-content white bg box-shadow-z2" role="main">


    @include('layouts.parts.nav')


  <div class="app-body" id="view">

    <div class="page-content">
      <div class="row-col">
        <div class="col-lg-9 b-r no-border-md">
          <div class="padding">


            <div class="page-title m-b">
              <h1 class="inline m-a-0">Artistas</h1>
              
            </div>
            <div data-ui-jp="jscroll" data-ui-options="{
            autoTrigger: false,
            loadingHtml: '<i class=\'fa fa-refresh fa-spin text-md text-muted\'></i>',
            padding: 50,
            nextSelector: 'a.jscroll-next:last'
          }">
              <div class="row row-lg">


                  @foreach($artistas as $artista)
                <div class="col-xs-4 col-sm-4 col-md-3">
                  <div class="item">
                    <div class="item-media rounded ">
                      <a href="{{ route('artista', $artista->id) }}" class="item-media-content" style="background-image: url('{{ asset($artista->imagen) }}');"></a>
                    </div>
                    <div class="item-info text-center">
                      <div class="item-title text-ellipsis">
                        <a href="{{ route('artista', $artista->id) }}">{{ $artista->nombre }}</a>
                       
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
        
              </div>
            </div>

          </div>
        </div>
        



        @include('layouts.parts.footer')
      </div>
    </div>


  </div>
</div>
@endsection