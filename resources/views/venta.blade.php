@extends('layouts.app')
@section('content')

<div id="content" class="app-content white bg" role="main">
    @include('layouts.parts.nav')
    <div class="app-body">

        <div class="row-col dark-white">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="p-a-lg text-center">
                    <h3 class="display-4 m-y-lg">{{ $album->nombre }}</h3>
                    <h5 class="display-5 m-y-lg">{{ $album->artista->nombre }}</h5>
                    <p class="text-muted text-md m-b-lg">Elige en que modalidad prefieres tu CD</p>

                </div>
            </div>
            <div class="col-md-3"></div>
        </div>


        <div class="row-col">
            <div class="col-sm-6">
                <div class="black cover cover-gd" style="background-image: url('{{ asset($album->imagen) }}');">
                    <div class="p-a-lg text-center">
                        <h3 class="display-3 m-y-lg">Fisico</h3>
                        <p class="text-muted text-md m-b-lg">Envio hasta tu domicilio</p>

                        <a href="#" class="btn circle white m-b-lg p-x-md" data-toggle="modal" data-target="#fisico-modal">Compra
                            Ya!</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 black lt">
                <div class="black cover cover-gd" style="background-image: url('{{ asset($album->imagen) }}');">
                    <div class="p-a-lg text-center">
                        <h3 class="display-3 m-y-lg">Digital</h3>
                        <p class="text-muted text-md m-b-lg">Descargalo ahora</p>
                        <a href="#" class="btn circle white m-b-lg p-x-md" data-toggle="modal" data-target="#digital-modal">Compra
                            Ya!</a>
                    </div>
                </div>
            </div>
        </div>


    </div>



</div>

<div id="fisico-modal" class="modal fade animate">
    <div class="modal-dialog">
        <div class="modal-content fade-down">

            <div class="modal-header">

                <h5 class="modal-title">Datos de facturación</h5>
                <a data-dismiss="modal" class="text-muted text-lg p-x modal-close-btn">&times;</a>
            </div>

            <div class="modal-body p-lg">
                <div class="form-group">
                    <input id="no_tarjeta" type="no_tarjeta" class="form-control{{ $errors->has('no_tarjeta') ? ' is-invalid' : '' }}"
                        name="no_tarjeta" value="{{ old('no_tarjeta') }}" required placeholder="Numero de Cuenta/Tarjeta">

                    @if ($errors->has('no_tarjeta'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('no_tarjeta') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="vencimiento" type="vencimiento" class="form-control{{ $errors->has('vencimiento') ? ' is-invalid' : '' }}"
                        name="vencimiento" value="{{ old('vencimiento') }}" required placeholder="Fecha Vencimiento">

                    @if ($errors->has('vencimiento'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('vencimiento') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="cvc" type="cvc" class="form-control{{ $errors->has('cvc') ? ' is-invalid' : '' }}" name="cvc"
                        value="{{ old('cvc') }}" required placeholder="CVC">

                    @if ($errors->has('cvc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cvc') }}</strong>
                    </span>
                    @endif
                </div>

            </div>

            <div class="modal-header">

                <h5 class="modal-title">Datos de envio</h5>
            </div>
            <div class="modal-body p-lg">
                <form method="POST" action="{{ route('ventaFisica') }}">
                    @csrf
                    <div class="form-group">
                        <input id="direccion" type="direccion" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}"
                            name="direccion" value="{{ old('direccion') }}" required placeholder="Direccion">

                        @if ($errors->has('direccion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('direccion') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input id="codigoPostal" type="codigoPostal" class="form-control{{ $errors->has('codigoPostal') ? ' is-invalid' : '' }}"
                            name="codigoPostal" value="{{ old('codigoPostal') }}" required placeholder="Codigo Postal">

                        @if ($errors->has('codigoPostal'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('codigoPostal') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input id="localidad" type="localidad" class="form-control{{ $errors->has('localidad') ? ' is-invalid' : '' }}"
                            name="localidad" value="{{ old('localidad') }}" required placeholder="Localidad">

                        @if ($errors->has('localidad'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('localidad') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input id="estado" type="estado" class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}"
                            name="estado" value="{{ old('estado') }}" required placeholder="Estado">

                        @if ($errors->has('estado'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('estado') }}</strong>
                        </span>
                        @endif
                    </div>
                    <br><h5 class="text-right">CD: ${{ $album->precio }}</h5>
                    <h5 class="text-right">Envio: $175.00</h5>
                    <h4 class="text-right">Total: ${{ $album->precio + 175 }}MNX</h4>
                    <button type="submit" class="btn btn-lg black p-x-lg">Confirmar</button>
                </form>

            </div>

        </div>
    </div>
</div>


<div id="digital-modal" class="modal fade animate">
    <div class="modal-dialog">
        <div class="modal-content fade-down">

            <div class="modal-header">

                <h5 class="modal-title">Datos de facturación</h5>
                <a data-dismiss="modal" class="text-muted text-lg p-x modal-close-btn">&times;</a>
            </div>

            <div class="modal-body p-lg">
                <div class="form-group">
                    <input id="no_tarjeta" type="no_tarjeta" class="form-control{{ $errors->has('no_tarjeta') ? ' is-invalid' : '' }}"
                        name="no_tarjeta" value="{{ old('no_tarjeta') }}" required placeholder="Numero de Cuenta/Tarjeta">

                    @if ($errors->has('no_tarjeta'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('no_tarjeta') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="vencimiento" type="vencimiento" class="form-control{{ $errors->has('vencimiento') ? ' is-invalid' : '' }}"
                        name="vencimiento" value="{{ old('vencimiento') }}" required placeholder="Fecha de Vencimiento">

                    @if ($errors->has('vencimiento'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('vencimiento') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="cvc" type="cvc" class="form-control{{ $errors->has('cvc') ? ' is-invalid' : '' }}" name="cvc"
                        value="{{ old('cvc') }}" required placeholder="CVC">

                    @if ($errors->has('cvc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cvc') }}</strong>
                    </span>
                    @endif
                </div>
                <br>
                <h4 class="text-right">Total: ${{ $album->precio }}MNX</h4>
                <button type="submit" class="btn btn-lg black p-x-lg">Confirmar</button>

            </div>




        </div>
    </div>
</div>


@endsection