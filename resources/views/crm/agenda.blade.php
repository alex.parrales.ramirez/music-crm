@extends('crm.layouts.app')
@section('content')


<div class="main-content">
    <!-- Top navbar -->

    @include('crm.layouts.parts.header')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Agenda</h3>
                    </div>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
        <!-- Dark table -->

        <!-- Footer -->
        @include('crm.layouts.parts.footer')


    </div>
</div>

@endsection

@section('css')
<link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" rel="stylesheet">
<link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>

<script>

  $(function() {

    $('#calendar').fullCalendar({
      defaultView: 'month',
      events: 'https://fullcalendar.io/demo-events.json'
    });

  });

</script>



@endsection