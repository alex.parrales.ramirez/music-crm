@extends('layouts.app')
@section('content')
<!-- content -->
<!-- content -->
<div id="content" class="app-content white bg box-shadow-z2" role="main">

    @include('layouts.parts.nav')
    <div class="app-body" id="view">
        <div class="pos-rlt">
            <div class="page-bg" data-stellar-ratio="2" style="background-image: url('{{ asset($artista->imagen) }}');"></div>
        </div>
        <div class="page-content">
            <div class="padding b-b">
                <div class="row-col">
                    <div class="col-sm w w-auto-xs m-b">
                        <div class="item w rounded">
                            <div class="item-media">
                                <div class="item-media-content" style="background-image: url('{{ asset($artista->imagen) }}');"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="p-l-md no-padding-xs">
                            <div class="page-title">
                                <h1 class="inline">{{ $artista->nombre }}</h1>
                            </div>
                            <p class="item-desc text-ellipsis text-muted" data-ui-toggle-class="text-ellipsis">{{ $artista->descripcion }}</p>
                            <div class="item-action m-b">

                                <span>9 Albums, 105 Tracks</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row-col">
                <div class="col-lg-9 b-r no-border-md">
                    <div class="padding">
                        <div class="nav-active-border b-primary bottom m-b-md">
                            <ul class="nav l-h-2x">
                                <li class="nav-item m-r inline">
                                    <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab_1">Visión
                                        general</a>
                                </li>

                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                
                                <h5 class="m-b">Albums</h5>
                                <div class="row m-b">

                                    @foreach($albums as $album)
                                    <div class="col-xs-4 col-sm-4 col-md-3">
                                        <div class="item r" data-id="item-{{ $album->id}}">
                                            <div class="item-media ">
                                                <a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ asset($album->imagen) }}');"></a>
                                            </div>
                                            <div class="item-info">
                                                <div class="item-title text-ellipsis">
                                                    <a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
                                                </div>
                                               
                                                <div class="item-meta text-sm text-muted">
                                                    <span class="item-meta-stats text-xs ">
                                                        ${{ $album->precio }}MNX
                                                    </span>
                                                </div>


                                            </div>
                                        </div>

                                       
                                    </div>
                                    @endforeach
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </div>


                @include('layouts.parts.footer')



            </div>
        </div>

        <!-- ############ PAGE END-->

    </div>
</div>
@endsection