@extends('layouts.app')
@section('content')

<!-- content -->
<div id="content" class="app-content white bg" role="main">

	@include('layouts.parts.nav')

	<div class="app-body" id="view">
		<div class="page-content">
			<div class="black dk">
				<div class="row no-gutter item-info-overlay">
					<div class="col-sm-6 text-white">
						<div class="owl-carousel owl-theme owl-dots-sm owl-dots-bottom-left " data-ui-jp="owlCarousel" data-ui-options="{
                     items: 1
                    ,loop: true
                    ,autoplay: true
                    ,nav: true
                    
				  }">
							@foreach($album_slide as $album)
							<div class="item r" data-id="item-{{ $album->id }}">
								<div class="item-media primary">
									<a href="#" class="item-media-content" style="background-image: url('{{ $album->imagen }}');"></a>
									<div class="item-overlay center">
										<a href="{{ route('album', $album->id) }}" class="btn-playpause">Play</a>
									</div>
								</div>
								<div class="item-info">
									<div class="item-title text-ellipsis">
										<a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
									</div>
									<div class="item-author text-sm text-ellipsis">
										<a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
									</div>
								</div>
							</div>

							@endforeach


						</div>
					</div>


					@foreach($album_new as $album)
					<div class="col-sm-3 col-xs-6">
						<div class="item r">
							<div class="item-media ">
								<a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ $album->imagen }}');"></a>
								<div class="item-overlay center">
										<a href="{{ route('album', $album->id) }}" class="btn-playpause">Play</a>
									</div>
							</div>
							<div class="item-info">
								
								<div class="item-title text-ellipsis">
									<a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
								</div>
								<div class="item-author text-sm text-ellipsis ">
									<a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
								</div>


							</div>
						</div>
					</div>
					@endforeach


				</div>
			</div>

			<div class="row-col">
				<div class="col-lg-9 b-r no-border-md">
					<div class="padding">
						<h2 class="widget-title h4 m-b">Tendencias</h2>
						<div class="owl-carousel owl-theme owl-dots-center" data-ui-jp="owlCarousel" data-ui-options="{
          margin: 20,
          responsiveClass:true,
          responsive:{
            0:{
              items: 2
            },
              543:{
                  items: 3
              }
          }
		}">

							@foreach($album_tendencia as $album)
							<div class="">
								<div class="item r" data-id="item-{{ $album->id }}">
									<div class="item-media item-media-4by3">
										<a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ $album->imagen }}');"></a>
										
									</div>
									<div class="item-info">
										<div class="item-overlay bottom text-right">
											<a href="#" >${{ $album->precio }}MNX</a>
										</div>
										<div class="item-title text-ellipsis">
											<a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
										</div>
										<div class="item-author text-sm text-ellipsis ">
											<a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
										</div>


									</div>
								</div>
							</div>
							@endforeach




						</div>
						<h2 class="widget-title h4 m-b">Nuevo</h2>
						<div class="row">
							@foreach($album_tendencia as $album)
							<div class="col-xs-4 col-sm-4 col-md-3">
								<div class="item r" data-id="item-{{ $album->id }}">
									<div class="item-media ">
										<a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ $album->imagen }}');"></a>
									
									</div>
									<div class="item-info">
										
										<div class="item-title text-ellipsis">
											<a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
										</div>
										<div class="item-author text-sm text-ellipsis ">
											<a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
										</div>


									</div>
								</div>
							</div>
							@endforeach




						</div>
						<h2 class="widget-title h4 m-b">Recomendado para ti</h2>
						<div class="row item-list item-list-md m-b">

							@foreach($album_recommended as $album)
							<div class="col-sm-6">
								<div class="item r" data-id="item-4">
									<div class="item-media ">
										<a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ $album->imagen }}');"></a>
										
									</div>
									<div class="item-info">
										
										<div class="item-title text-ellipsis">
											<a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
										</div>
										<div class="item-author text-sm text-ellipsis ">
											<a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
										</div>
									</div>
								</div>
							</div>
							@endforeach


						</div>
					</div>
				</div>
				@include('layouts.parts.footer')
			</div>
		</div>

		<!-- ############ PAGE END-->

	</div>
</div>
<!-- / -->

<!-- ############ SWITHCHER START-->
<div id="switcher">
	<div class="switcher white" id="sw-theme">
		<a href="#" data-ui-toggle-class="active" data-ui-target="#sw-theme" class="white sw-btn">
			<i class="fa fa-gear text-muted"></i>
		</a>
		<div class="box-header">
			<strong>Theme Switcher</strong>
		</div>
		<div class="box-divider"></div>
		<div class="box-body">
			<p id="settingLayout" class="hidden-md-down">
				<label class="md-check m-y-xs" data-target="container">
					<input type="checkbox">
					<i class="green"></i>
					<span>Boxed Layout</span>
				</label>
				<label class="m-y-xs pointer" data-ui-fullscreen data-target="fullscreen">
					<span class="fa fa-expand fa-fw m-r-xs"></span>
					<span>Fullscreen Mode</span>
				</label>
			</p>
			<p>Colors:</p>
			<p data-target="color">
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="primary">
					<i class="primary"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="accent">
					<i class="accent"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="warn">
					<i class="warn"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="success">
					<i class="success"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="info">
					<i class="info"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="blue">
					<i class="blue"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="warning">
					<i class="warning"></i>
				</label>
				<label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
					<input type="radio" name="color" value="danger">
					<i class="danger"></i>
				</label>
			</p>
			<p>Themes:</p>
			<div data-target="bg" class="text-u-c text-center _600 clearfix">
				<label class="p-a col-xs-3 light pointer m-a-0">
					<input type="radio" name="theme" value="" hidden>
					<i class="active-checked fa fa-check"></i>
				</label>
				<label class="p-a col-xs-3 grey pointer m-a-0">
					<input type="radio" name="theme" value="grey" hidden>
					<i class="active-checked fa fa-check"></i>
				</label>
				<label class="p-a col-xs-3 dark pointer m-a-0">
					<input type="radio" name="theme" value="dark" hidden>
					<i class="active-checked fa fa-check"></i>
				</label>
				<label class="p-a col-xs-3 black pointer m-a-0">
					<input type="radio" name="theme" value="black" hidden>
					<i class="active-checked fa fa-check"></i>
				</label>
			</div>
		</div>
	</div>
</div>
<!-- ############ SWITHCHER END-->

<!-- ############ SHARE START -->
<div id="share-modal" class="modal fade animate">
	<div class="modal-dialog">
		<div class="modal-content fade-down">
			<div class="modal-header">

				<h5 class="modal-title">Share</h5>
			</div>
			<div class="modal-body p-lg">
				<div id="share-list" class="m-b">
					<a href="" class="btn btn-icon btn-social rounded btn-social-colored indigo" title="Facebook">
						<i class="fa fa-facebook"></i>
						<i class="fa fa-facebook"></i>
					</a>

					<a href="" class="btn btn-icon btn-social rounded btn-social-colored light-blue" title="Twitter">
						<i class="fa fa-twitter"></i>
						<i class="fa fa-twitter"></i>
					</a>

					<a href="" class="btn btn-icon btn-social rounded btn-social-colored red-600" title="Google+">
						<i class="fa fa-google-plus"></i>
						<i class="fa fa-google-plus"></i>
					</a>

					<a href="" class="btn btn-icon btn-social rounded btn-social-colored blue-grey-600" title="Trumblr">
						<i class="fa fa-tumblr"></i>
						<i class="fa fa-tumblr"></i>
					</a>

					<a href="" class="btn btn-icon btn-social rounded btn-social-colored red-700" title="Pinterst">
						<i class="fa fa-pinterest"></i>
						<i class="fa fa-pinterest"></i>
					</a>
				</div>
				<div>
					<input class="form-control" value="http://plamusic.com/slug" />
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ############ SHARE END -->

<!-- ############ LAYOUT END-->

@endsection