<div class="app-header white lt box-shadow-z1">
    <div class="navbar" data-pjax>
        <a data-toggle="collapse" data-target="#navbar" class="navbar-item pull-right hidden-md-up m-r-0 m-l">
            <i class="material-icons">menu</i>
        </a>
        <!-- brand -->
        <a href="{{ route('index') }}" class="navbar-brand md">
            <img src="{{ asset('images/logo.png') }}" alt=".">
            <span class="hidden-folded inline">Music Sound</span>
        </a>
        <!-- / brand -->

        <!-- nabar right -->
        <ul class="nav navbar-nav pull-right">

            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#search-modal">
                    <i class="material-icons">search</i>
                </a>
            </li>
            @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">
                    <span class="hidden-xs-down btn btn-sm rounded primary _600">
                        Inicia sesión
                    </span>
                    <span class="hidden-sm-up btn btn-sm btn-icon rounded primary">
                        <i class="material-icons">person_add</i>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('register') }}" class="nav-link">
                    <span class="nav-text">Registrate</span>
                </a>
            </li>
            @else
            <li class="nav-item dropdown">
                <a href="#" class="nav-link clear" data-toggle="dropdown">
                    <span class="avatar w-32">
                        <img src="{{ asset('images/user.png') }}" alt="...">
                    </span>
                </a>
                <div class="dropdown-menu w dropdown-menu-scale pull-right">
                    <a class="dropdown-item" href="#">
                        <span>Perfil</span>
                    </a>
                   
                    <a class="dropdown-item" href="{{ route('dashboard') }}">
                        <span>Administrador</span>
                    </a>
                    <div class="dropdown-divider"></div>


                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Cerrar Sesión</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </div>
            </li>
            @endguest
        </ul>
        <!-- / navbar right -->

        <!-- navbar collapse -->
        <div class="collapse navbar-toggleable-sm l-h-0 text-center" id="navbar">
            <!-- link and dropdown -->
            <ul class="nav navbar-nav nav-md inline text-primary-hover" data-ui-nav>
                <li class="nav-item">
                    <a href="{{ route('index') }}" class="nav-link">
                        <span class="nav-text">Descubre</span>
                    </a>
                </li>
               
                <li class="nav-item dropdown pos-stc">
                    <a href="{{ route('albums') }}" class="nav-link">
                        <span class="nav-text">Albums</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('artistas') }}" class="nav-link">
                        <span class="nav-text">Artistas</span>
                    </a>
                </li>
            </ul>
            <!-- / link and dropdown -->
        </div>
        <!-- / navbar collapse -->
    </div>
</div>