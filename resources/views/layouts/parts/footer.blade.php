<div class="col-lg-3 w-xxl w-auto-md">
    <div class="padding" style="bottom: 60px;" data-ui-jp="stick_in_parent">
        <h6 class="text text-muted">Top 5</h6>
        <div class="row item-list item-list-sm m-b">

            @foreach($album_top as $album)
            <div class="col-xs-12">
                <div class="item r" data-id="item-{{ $album->id }}">
                    <div class="item-media ">
                        <a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ asset($album->imagen) }}');"></a>
                    </div>
                    <div class="item-info">
                        <div class="item-title text-ellipsis">
                            <a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
                        </div>
                        <div class="item-author text-sm text-ellipsis ">
                            <a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
                        </div>


                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <h6 class="text text-muted">Apps</h6>
        <div class="btn-groups">
            <a href="#" class="btn btn-sm dark lt m-r-xs" style="width: 135px">
                <span class="pull-left m-r-sm">
                    <i class="fa fa-apple fa-2x"></i>
                </span>
                <span class="clear text-left l-h-1x">
                    <span class="text-muted text-xxs">Descarga en la </span>
                    <b class="block m-b-xs">App Store</b>
                </span>
            </a>
            <a href="#" class="btn btn-sm dark lt" style="width: 133px">
                <span class="pull-left m-r-sm">
                    <i class="fa fa-play fa-2x"></i>
                </span>
                <span class="clear text-left l-h-1x">
                    <span class="text-muted text-xxs">Disponible en</span>
                    <b class="block m-b-xs m-r-xs">Google Play</b>
                </span>
            </a>
        </div>
        <div class="b-b m-y"></div>
        <div class="nav text-sm _600">
            <a href="#" class="nav-link text-muted m-r-xs">Acerca</a>
            <a href="#" class="nav-link text-muted m-r-xs">Contacto</a>
            <a href="#" class="nav-link text-muted m-r-xs">Legal</a>
            <a href="#" class="nav-link text-muted m-r-xs">Politica</a>
        </div>
        <p class="text-muted text-xs p-b-lg">&copy; Copyright 2016</p>
    </div>
</div>