<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistaCancionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artista_cancion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artista_id')->unsigned();
            $table->integer('cancion_id')->unsigned();
            $table->timestamps();

            $table->foreign('artista_id')->references('id')->on('artistas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        $table->foreign('cancion_id')->references('id')->on('cancions')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artista_cancion');
    }
}
